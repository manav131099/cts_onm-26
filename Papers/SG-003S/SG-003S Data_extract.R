rm(list=ls(all =TRUE))

pathRead <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Raw Data/[SG-003S]"
pathWrite <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/SG-003S_PR_summary.txt"
pathWrite2 <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/SG-003S_PR_summary.csv"
setwd(pathRead)                                     #set working directory
print("Extracting data..")
filelist <- dir(pattern = ".txt", recursive= TRUE)  #contains only files of '.txt' format

nameofStation <- "SG-003S"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()


col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = 0


index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")  #reading text file at location filelist[i]
  
  if(identical(nchar(i), as.integer(31))){
    
    for (j in 1:nrow(temp)){
      
      col0[index] <- nameofStation
      
      col1[index] <- paste0(temp[j,1])
      
      col2[index] <- as.numeric(temp[j,26])
      
      col3[index] <- as.numeric(temp[j,27])
      
      col4[index] <- as.numeric(temp[j,28])
      
      index <- index + 1
      
    }
    
    print(paste(i, "done"))
    
  }

}

col0 <- col0[1:(index-1)]   #removes last row for all columns
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col1[is.na(col1)] <- NA              #states T/F
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col2[which(col2==0)] = NA
col3[which(col3==0)] = NA
col4[which(col4==0)] = NA
# col2[c(186,337,514,301,56,410,187,103,724)] = NA 
# col3[c(328,98,161,171,337,514,56,301,55)] = NA           
# col4[c(630,483,194,708,55,337,514,301,112)] = NA
# col4[c(55:111)] = NA #57 point 

exclude_index <- 1
for(i in col2){
  if(isTRUE (i < 60)){  # eliminate PR below 60%
    col2[exclude_index] <- NA
  }
  
  if(isTRUE (i > 100)){   # eliminate PR above 100%
    col2[exclude_index] <- NA
  }
  exclude_index <- exclude_index +1
}

exclude_index <- 1
for(i in col3){
  if(isTRUE (i < 60)){  # eliminate PR below 60%
    col3[exclude_index] <- NA
  }
  
  if(isTRUE (i > 100)){   # eliminate PR above 100%
    col3[exclude_index] <- NA
  }
  exclude_index <- exclude_index +1
}

exclude_index <- 1
for(i in col4){
  if(isTRUE (i < 50)){  # eliminate PR below 50%
    col4[exclude_index] <- NA
  }
  
  if(isTRUE (i > 100)){   # eliminate PR above 100%
    col4[exclude_index] <- NA
  }
  exclude_index <- exclude_index +1
}


print("Starting to save..")

result <- cbind(col0,col1,col2,col3,col4)
colnames(result) <- c("Meter Reference","Date","PR MA","PR MB","PR MC")         #columns names


for(x in c(3,4,5)){
  result[,x] <- round(as.numeric(result[,x]),1)

}

rownames(result) <- NULL
result <- data.frame(result)
result <- result[-c(1:54),] # remove row 1 to 54
for(i in 1:nrow(result)){
  result[i,6] <- i
}
colnames(result) <- c("Meter Reference","Date","PR MA","PR MB","PR MC","No. of Days")
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")   #saving into txt and csv file
write.csv(result,pathWrite2, na= "", row.names = FALSE)