require('httr')
source('/home/admin/CODE/MasterMail/timestamp.R')

bucketTime = function(time)
{
	splitData = unlist(strsplit(time,"\\ "))
	seq1 = seq(from = 2,to=length(splitData),by=2)
	timeAc = splitData[seq1]
	timeSplitData = unlist(strsplit(timeAc,":"))
	seq1 = seq(from = 1,to=length(timeSplitData),by=2)
	numericValues = (as.numeric(timeSplitData[seq1])*60)+as.numeric(timeSplitData[(seq1+1)])
	numericValues = floor(numericValues/5)
	numericValues = numericValues + 1
	return(numericValues)
}

cleansedata = function(pth)
{
	df2a = list()
	df = read.table(pth,header = T,sep="\t")
	listofstrngs = c("TEXMO-A","TEXMO-B")
	dfcpy = df
	for(outer in 1 : 2)
	{
	df = dfcpy
  idxuse = which(as.character(df[,13]) %in% listofstrngs[outer])
	idxuse = idxuse[complete.cases(idxuse)]
	if(length(idxuse) < 1 )
	{
		df2 = data.frame(Tm=NA,Pac=NA,Eac=NA,Irr=NA,Tmod=NA)
		df2a[[outer]] = df2
		next
	}
	df = df[idxuse,]
	c2 = as.numeric(df[,17])
	c4 = as.numeric(df[,16])
	c10 = as.character(df[,10])
	c1idx = which(c10 %in% "m")
  c2idx = which(c10 %in% "n")
	c1 = as.character(df[,19])
  c2 = as.character(df[,18])
  c3 = as.character(df[,17])
	c4 = as.character(df[,16])
	c5 = as.character(df[,15])
  {
	if(length(c1idx) > 1)
	{
	c1 = as.character(df[c1idx,19])
  c2 = as.character(df[c1idx,18])
  c3 = as.character(df[c1idx,17])
	idx = bucketTime(c1)
	index = match(unique(idx),idx)
	print(paste('Length of index1',length(index)))
	c1 = c1[index]
	c2 = c2[index]
	c3 = c3[index]
  }
	else
	{
	 print('Error c1,c2,c3 has no data')
	 c1=c2=c3=NA
	}
	}
	print(paste('Length of c1 is',length(c1)))
	{
	if(length(c2idx) > 1)
	{
  c4 = as.character(df[c2idx,16])
  c5 = as.character(df[c2idx,15])
	Time = as.character(df[c2idx,19])
	idx = bucketTime(Time)
	index = match(unique(idx),idx)
	print(paste('Length of index2',length(index)))
	Time = Time[index]
	c4 = c4[index]
	c5 = c5[index]
	{
	if(length(Time) > length(c1))
	{
		print(paste('Length missmatch c1 length',length(c1),'c4 length',length(c4)))
		index = match(bucketTime(c1),bucketTime(Time))
		idxNAs = which(index %in% NA)
		print(paste('length of idxnas',length(idxNAs)))
		print(paste('length of index prior to cleanup',length(index)))
		index = index[complete.cases(index)]
		print(paste('length of index after cleanup',length(index)))
		c4 = c4[index]
		c5 = c5[index]
		print(paste('Length before cleansing, c1:',length(c1),'c4:',length(c4)))
		print(paste('idxas is',idxNAs))
		if(length(idxNAs))
		{
		for(inner in 1 : length(idxNAs))
		{
			oldlen = length(c4)
			if(idxNAs[inner] <= oldlen)
			{
				c4[(idxNAs[inner]+1) : (oldlen+1)] = c4[idxNAs[inner] : oldlen]
				c5[(idxNAs[inner]+1) : (oldlen+1)] = c5[idxNAs[inner] : oldlen]
			}
			c4[idxNAs[inner]] = c5[idxNAs[inner]] = NA
		}
		  #c4 = c4[1:length(c1)]
			#c5 = c5[1:length(c1)]
		}
		print(paste('Length after cleansing, c1:',length(c1),'c4:',length(c4)))
	}
	else if (length(Time) < length(c1))
	{
		print(paste('Length missmatch c1 length',length(c1),'c4 length',length(c4)))
		index = match(bucketTime(Time),bucketTime(c1))
		print(paste('index length prior to cleanup',length(index)))
		index = index[complete.cases(index)]
		c4 = c4[complete.cases(c4)]
		c5 = c5[complete.cases(c5)]
		idxactual = c(1:length(c1))
		c4cp = c5cp = c()
		c4cp[idxactual] = NA
		c5cp[idxactual] = NA
		c4cp[index] = c4
		c5cp[index] = c5
		idxactual = idxactual[-index]
		print(paste('index length:',length(index),'idxactual length:',length(idxactual)))
		c4 = c4cp
		c5 = c5cp
		print(paste('Length after cleansing, c1:',length(c1),'c4:',length(c4)))
	}
	else
		print('Success c1 and c4 have same length')
	}
	}
	else
	{
		c4 = c5=unlist(rep(NA,length(c1)))
		print('Reverting to default')
	}
	}
	print(paste('c1 length is',length(c1),'c4 length is',length(c4)))
	df2 = data.frame(Tm=c1,Pac=c2,Eac=c3,Irr=c4,Tmod=c5)
	df2a[[outer]] = df2
	recordTimeMaster("IN-031T","Gen1",as.character(df2[nrow(df2),1]))
  }
	return(df2a)
}
pathCentral = '/home/admin/Data/TORP Data/AllStations'

probePath = function(day)
{
	path = pathCentral
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	#checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	#checkDir(pathYr)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	{
		print(pathFinal)
		if(file.exists(pathFinal))
		{
			df = read.table(pathFinal,header = T,sep = "\t")
			return(df)
		}
		else
			return(NULL)
	}
}

fetchrawdata = function(day1,day2)
{
  	#fetch data from SERVER USING A POST-request
		masterData = probePath(day1)
		
		if(is.null(masterData))
		{
			return(masterData)
		}
		stnnames = as.character(masterData[,13])
		subst = which(stnnames %in% c("TEXMO-A","TEXMO-B"))
		if(length(subst))
		{
			vals2 = masterData[subst,] # extract only rows with MJ Logistics tag in column 13
			recordTimeMaster("IN-031T","FTPNewFiles",as.character(vals2[nrow(vals2),19]))
			return(vals2)
		}
		return(NULL)
}

