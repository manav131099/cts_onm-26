errHandle = file('/home/admin/Logs/LogsKH004Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/KH004Digest/HistoricalAnalysis2G3GKH004.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/KH004Digest/aggregateInfo.R')
initDigest = function(df,no,extra)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'MSB-01 Loads'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'MSB-01 Solar'
		noac="1"
	}
	else if(as.numeric(no)==3)
	{
		no2 = "MSB-02 Loads"
		noac=""
	}
	else if(as.numeric(no)==4)
	{
		no2 = "MSB-02 Solar"
		noac="2"
	}
	}
	#ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1])," (",
	substr(as.character(weekdays(as.Date(as.character(df[,1])))),1,3),") ",no2,sep="")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Eac",noac,"-1 [kWh]: ",as.character(df[,2]),sep="")
	{
	if(as.numeric(no)==2 || as.numeric(no)==4)
	{
		body = paste(body,"\n\nYield-1 [kWh/kWp]: ",as.character(df[,9]),sep="")
		body = paste(body,"\n\nEac",noac,"-2 [kWh]: ",as.character(df[,3]),sep="")
		body = paste(body,"\n\nYield-2 [kWh/kWp]: ",as.character(df[,10]),sep="")
		body = paste(body,"\n\nPR-1 [%]: ",as.character(df[,11]),sep="")
		body = paste(body,"\n\nPR-2 [%]: ",as.character(df[,12]),sep="")
  	body = paste(body,"\n\nRatio [%]: ",as.character(df[,4]),sep="")
		acpts = round(as.numeric(df[,5]) * 14.4)
  	body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,5]),"%)",sep="")
		body = paste(body,"\n\nDowntime (%): ",as.character(df[,6]),sep="")
  	body = paste(body,"\n\nLast recorded timestamp: ",as.character(df[,7]),sep="")
  	body = paste(body,"\n\nLast recorded energy meter reading [kWh]: ",as.character(df[,8]),sep="")
	}
	else if(as.numeric(no)==1 || as.numeric(no)==3)
	{
		body = paste(body,"\n\nExport Amount [kWh] :",as.character(df[,10]))
		body = paste(body,"\n\nExport Percentage [%] :",extra)
		acpts = round(as.numeric(df[,3])*14.4)
  	body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,3]),"%)",sep="")
	}
	}
  return(body)
}

printtsfaults = function(TS,num,body)
{
  no = num
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'MSB-01 Loads'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'MSB-01 Solar'
		noac="1"
	}
	else if(as.numeric(no)==3)
	{
		no2 = "MSB-02 Loads"
		noac=""
	}
	else if(as.numeric(no)==4)
	{
		no2 = "MSB-02 Solar"
		noac="2"
	}
	}
	num = no2
	if(length(TS) > 1)
	{
		if(as.numeric(no)==2 || as.numeric(no)==4)
		{
		body = paste(body,"\n\n_________________________________________\n\n",sep="")
		body = paste(body,paste("Timestamps for",num,"where Pac < 1 between 8am -5pm\n"),sep="")
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		}
	}
	return(body)
}

sendMail = function(df1,df2,df3,df4,pth1,pth2,pth3,pth4,pth5)
{
  filetosendpath = c(pth1,pth2,pth3,pth4,pth5)
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
#	data3G = read.table(pth3,header =T,sep = "\t")
#	dateineed = unlist(strsplit(filenams[1]," "))
#	dateineed = unlist(strsplit(dateineed[2],"\\."))
#	dateineed = dateineed[1] 
#	idxineed = match(dateineed,as.character(data3G[,1]))
	print('Filenames Processed')
	body=""
	FSGeneration = Eac1Recorded + Eac2Recorded
	FSPR = round((FSGeneration/5.4432/GModRecorded),1)
	FSPRPy = round((FSGeneration/5.4432/GPyRecorded),1)
	STDDEVYLD = sdp(c(Yld1Recorded,Yld2Recorded))
	COVYLD = STDDEVYLD*100 / mean(c(Yld1Recorded,Yld2Recorded))
	body = paste(body,"Site Name: Angkor Dairy",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia",sep="")
	body = paste(body,"\n\nO&M Code: KH-004",sep="")
	body = paste(body,"\n\nSystem Size: 544.32 kWp",sep="")
	body = paste(body,"\n\nNumber of Energy Meters: 2",sep="")
	body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / CS6K-270Wp / 1728 Trina Solar/ TSM-PD05 270/288",sep="")
	body = paste(body,"\n\nInverter Brand / Model / Nos:  SMA / STP60-10 / 6",sep="")
	body = paste(body,"\n\nSite COD: ",DOB,sep="")
	body = paste(body,"\n\nSystem age [days]: ",as.character((as.numeric(DAYSALIVE))),sep="")
	body = paste(body,"\n\nSystem age [years]: ",as.character(round((as.numeric(DAYSALIVE))/365,2)),sep="")
	body = paste(body,"\n\nFull site generation [kWh]: ",as.character(round(FSGeneration,2)),sep="")
	body = paste(body,"\n\nFull site yield [kWh/kWp]: ",as.character(round(FSGeneration/544.32,2)),sep="")
	body = paste(body,"\n\nTotal daily irradiation (from KH-003S) [kWh/m2]: ",df2[,13],sep="")
	body = paste(body,"\n\nTotal daily irradiation (from KH-714S) [kWh/m2]: ",df2[,17],sep="")
	body = paste(body,"\n\nDate: ",as.character(df2[,1])," (",
	substr(as.character(weekdays(as.Date(as.character(df2[,1])))),1,3),
	")",sep="")
	body = paste(body,"\n\nFull site PR [%]: ",as.character(round(FSPR,2)),sep="")
	body = paste(body,"\n\nFull site PR-Pyranometer [%]: ",as.character(round(FSPRPy,2)),sep="")
	body = paste(body,"\n\nStdev yield: ",as.character(round(STDDEVYLD,2)),sep="")
	body = paste(body,"\n\nCOV yield [%]: ",as.character(round(COVYLD,1)),sep="")
  artLoad = as.numeric(df2[,3]) + as.numeric(df4[,3]) + as.numeric(df1[,2]) + as.numeric(df3[,2])
	solCont = round((as.numeric(df2[,3]) + as.numeric(df4[,3]))*100/artLoad,1)
	expPerc1 = round(as.numeric(df1[,10])*100/as.numeric(df2[,3]),1)
	expPerc3 = round(as.numeric(df3[,10])*100/as.numeric(df4[,3]),1)
	totExp = as.numeric(df1[,10]) + as.numeric(df3[,10])
	ExpPercGlob = round(totExp*100/FSGeneration,1)
	body = paste(body,"\n\nArtificial load [kWh]: ",artLoad,sep="")
	body = paste(body,"\n\nSolar contribution [%]: ",solCont,sep="")
	body = paste(body,"\n\nExport Amount [kWh]: ",totExp,sep="")
	body = paste(body,"\n\nExport Percentage [%]: ",ExpPercGlob,sep="")
	body = paste(body,initDigest(df2,2,NA))  #its correct, dont change
	body = paste(body,initDigest(df4,4,NA))  #its correct, dont change
	body = paste(body,initDigest(df1,1,expPerc1))  #its correct, dont change
	body = paste(body,initDigest(df3,3,expPerc3))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,1,body)
  body = printtsfaults(TIMESTAMPSALARM2,2,body)
  body = printtsfaults(TIMESTAMPSALARM3,3,body)
  body = printtsfaults(TIMESTAMPSALARM4,4,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________\n\n",sep="")
  body = paste(body,"Station Data",sep="")
  body = paste(body,"\n\n_________________________________________\n\n",sep="")
  body = paste(body,"Station DOB: ",as.character(DOB),sep="")
  body = paste(body,"\n\n# Days alive: ",as.character(DAYSALIVE),sep="")
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive: ",yrsalive,sep="")
  graph_command1=paste("Rscript /home/admin/CODE/EmailGraphs/PR_Graph_Azure.R","KH-004",61.8,0.008,"2017-03-14",substr(currday,14,23),sep=" ")
  system(graph_command1)
  graph_path1=paste('/home/admin/Graphs/Graph_Output/KH-004/[KH-004] Graph ',substr(currday,14,23),' - PR Evolution.pdf',sep="")
  graph_extract_path1=paste('/home/admin/Graphs/Graph_Extract/KH-004/[KH-004] Graph ',substr(currday,14,23),' - PR Evolution.txt',sep="")

  graph_command1=paste('python3 /home/admin/CODE/EmailGraphs/Meter_CoV_Graph.py "KH-004"',substr(currday,14,23),3,sep=" ")
  system(graph_command1)
  graph_path2=paste('/home/admin/Graphs/Graph_Output/KH-004/[KH-004] Graph ',substr(currday,14,23),' - Meter CoV.pdf',sep="")
  graph_extract_path2=paste('/home/admin/Graphs/Graph_Extract/KH-004/[KH-004] Graph ',substr(currday,14,23),' - Meter CoV.txt',sep="")

  filetosendpath=c(graph_path1,graph_extract_path1,graph_path2,graph_extract_path2,filetosendpath)
	print('3G data processed')
	body = gsub("\n ","\n",body)
	while(1)
	{
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-004X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		Sys.sleep(180)
		next
	}
	break
	}
	recordTimeMaster("KH-004X","Mail",substr(currday,14,23))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
#recipients = c('andre.nobre@cleantechsolar.com','rupesh.baker@cleantechsolar.com','lucas.ferrand@cleantechsolar.com',
#'Thareth.Song@comin.com.kh','Sothea.Hin@comin.com.kh')
recipients = getRecipients("KH-004X","m")
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("KH-004X","m")
  recordTimeMaster("KH-004X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[KH-004X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			#if(length(stations) > 2)
			#stations = c(stations[2],stations[3],stations[1])
			pathdayscomap = paste(pathmonths,stations[1],sep="/")
			pathdayscomap2 = paste(pathmonths,stations[2],sep="/")
      pathdays = paste(pathmonths,stations[3],sep="/")
      pathdays2 = paste(pathmonths,stations[4],sep="/")
      pathdays3 = paste(pathmonths,stations[5],sep="/")
      pathdays4 = paste(pathmonths,stations[6],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[3],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[4],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[5],sep="/")
      writepath2Gdays4 = paste(writepath2Gmon,stations[6],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      checkdir(writepath2Gdays3)
      checkdir(writepath2Gdays4)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      days3 = dir(pathdays3)
      days4 = dir(pathdays4)
			dayscomap = dir(pathdayscomap)
			dayscomap2 = dir(pathdayscomap2)

      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t],days4[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t],days4[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]) && is.na(days4[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
				 if(!(condn1 || condn2))
				 {
				 	sendmail = 0
				 }
				 else
				 {
         	DAYSALIVE = DAYSALIVE + 1
				 }
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 print(paste('Processing',days3[t]))
				 print(paste('Processing',days4[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
         writepath2Gfinal4 = paste(writepath2Gdays4,"/",days4[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
         readpath3 = paste(pathdays3,days3[t],sep="/")
         readpath4 = paste(pathdays4,days4[t],sep="/")
				 FILEPATHCOMP = paste(pathdayscomap,dayscomap[t],sep="/")
				 resetGlobals()
				 METERCALLED <<- 1  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 METERCALLED <<- 3 # its correct dont change
				 FILEPATHCOMP = paste(pathdayscomap2,dayscomap2[t],sep="/")
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 METERCALLED <<- 4 # its correct dont change
         df4 = secondGenData(readpath4,writepath2Gfinal4)
				 datemtch = unlist(strsplit(days[t]," "))
				thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath2Gfinal4,writepath3Gfinal)
  if(sendmail ==0)
  {
	  Sys.sleep(3600)
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,df3,df4,writepath2Gfinal,writepath2Gfinal2,
	writepath2Gfinal3,writepath2Gfinal4,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
