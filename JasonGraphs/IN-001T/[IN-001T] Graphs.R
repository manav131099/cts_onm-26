source('C:/Users/talki/Desktop/cec intern/codes/IN-001T/[IN-001T] Data extraction.R')

rm(list=ls(all =TRUE))
library(ggplot2)

pathWrite <- "C:/Users/talki/Desktop/cec intern/results/IN-001T/"
result <- read.csv("C:/Users/talki/Desktop/cec intern/results/IN-001T/[IN-001T]_summary.csv")
station <- "IN-001"
rownames(result) <- NULL
#result <- result[-length(result[,1]),]
result <- result[,-1]

colnames(result) <- c("date","da","pts","uptime","wuptime","pr")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
result[,2] <- as.numeric(paste(result[,2]))
result[,3] <- as.numeric(paste(result[,3]))
result[,4] <- as.numeric(paste(result[,4]))
result[,5] <- as.numeric(paste(result[,5]))
result[,6] <- as.numeric(paste(result[,6]))
date <- result[,1]

dagraph <- ggplot(result, aes(x=date,y=da))+ylab("Data Availability [%]")
daFinalGraph<- dagraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  ggtitle(paste0("[",station,"] Data Availability"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(panel.grid.minor = element_blank()) +
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 112)+
  annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 107)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

daFinalGraph
ggsave(daFinalGraph,filename = paste0(pathWrite,station,"_DA_LC.pdf"),width =7.92, height =5)


uptimeGraph <- ggplot(result, aes(x=date,y=uptime))+ylab("Uptime [%]")
uptimeFinalGraph<- uptimeGraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  coord_cartesian(ylim=c(0,105))+
  ggtitle(paste0("Uptime for [",station,"]"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Uptime Percentage, Pac > 0.1 kW = ", format(round(mean(result[,4][!is.na(result[,4])]),1),nsmall=1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 45)+
  annotate("text",label = paste0("System Lifetime = ", length(date)," days (",round(length(date)/365,1)," years)      "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 41)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

uptimeFinalGraph
ggsave(uptimeFinalGraph,filename = paste0(pathWrite,station,"_PAC_LC.pdf"), width =7.92, height =5)



wuptimeGraph <- ggplot(result, aes(x=date,y=wuptime)) + ylab("Weighted uptime [%]")
wuptimeFinalGraph <- wuptimeGraph + geom_line(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 115, 10))+
  coord_cartesian(ylim=c(0,105))+
  ggtitle(paste0("Irradiance - weighted uptime for [",station,"]"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  annotate("text",label = paste0("Weighted uptime Percentage, Pac > 0.1 kW = ", format(round(mean(result[,5][!is.na(result[,5])]),1),nsmall=1),"%"),size=3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 25)+
  annotate("text",label = paste0("System Lifetime = ", length(date)," days (",round(length(date)/365,1)," years)                     "),size = 3.6,
           x = as.Date(date[round(0.518*length(date))]), y= 21)+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

wuptimeFinalGraph
ggsave(wuptimeFinalGraph,filename = paste0(pathWrite,station,"_WPAC_LC.pdf"),width =7.92, height =5)


prGraph <- ggplot(result, aes(x=date,y=pr))+ylab("Performance Ratio [%]")
prFinalGraph<- prGraph + geom_point(size=0.5)+
  theme_bw()+
  expand_limits(x=date[1],y=0)+
  scale_x_date(date_breaks = "3 month",date_labels = "%b")+
  scale_y_continuous(breaks=seq(0, 100, 10))+
  coord_cartesian(ylim=c(0,100))+
  ggtitle(paste0("PR1 for [",station,"]"), subtitle = paste0("From ",date[1]," to ",date[length(date)])) +
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))+
  theme(plot.title=element_text(margin=margin(0,0,7,0)))+
  geom_hline(yintercept=mean(result[,6][result[,6]<100&(!is.na(result[,6]))]),size=0.3)+
  annotate("text", label = paste0("Average PR = ", round(mean(result[,6][result[,6]<100&(!is.na(result[,6]))]),1),"%"), size = 3.6, 
                                 x = as.Date(date[round(0.51*length(date))]), y=3 , color="black")+
  theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

prFinalGraph

ggsave(prFinalGraph,filename = paste0(pathWrite,station,"_PR_LC.pdf"),width =7.92, height =5)
