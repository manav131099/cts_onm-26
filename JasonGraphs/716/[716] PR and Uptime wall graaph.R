source('C:/Users/talki/Desktop/cec intern/codes/716/[716] PR and Uptime data extract for uptime.R')

rm(list=ls(all =TRUE))
library(ggplot2)
library(zoo)

ma <- function(x,y,del)
{
  v1 <- zoo(x,as.Date(y))
  v2 <- rollapplyr(v1, (-(del:1)), mean, fill = NA, na.rm = TRUE)
  return(v2)
}

pathWrite <- "C:/Users/talki/Desktop/cec intern/results/716/uptime/"
result <- read.csv("C:/Users/talki/Desktop/cec intern/results/716/uptime/[SG-003]_summary 1.csv")
result <- result[-length(result[,1]),]
station <- "[716]"
rownames(result) <- NULL
result <- result[,-1]

colnames(result) <- c("date","da","pts","uptime1","wuptime1","pr1","uptime2",'wuptime2',"pr2","uptime3","wuptime3","pr3","prt")
result[,1] <- as.Date(result[,1], origin = result[,1][1])
for(i in 2:13){
  result[,i] <- as.numeric(paste(result[,i]))
}
date <- result[,1]
yax=120

#MOVING AVERAGE

zoo.pr1 <- zoo(result[,7], as.Date(result$date))
ma1 <- rollapplyr(zoo.pr1,list(-(29:1)), mean, fill=NA, na.rm=T)
result$ambpr1.av=coredata(ma1)

themesettings <-   theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_blank())+
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(plot.title = element_text(face = "bold",size= 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)), plot.subtitle = element_text(face = "bold",size= 12,lineheight = 0.9,hjust = 0.5))

dagraph <- ggplot(result, aes(x=date,y=da)) + ylab("Data Availability [%]")
da <- dagraph + geom_line(size=0.5)
da <- da + theme_bw() + expand_limits(x=date[1],y=0)
da <- da + scale_y_continuous(breaks=seq(0, 100, 10))
da <- da + scale_x_date(date_breaks = "month",date_labels = "%b")
da <- da + ggtitle(paste0("",station," Data Availability"), subtitle = paste0("From ",date[1]," to ",date[length(date)]))
da <- da + themesettings 
da <- da + annotate("text",label = paste0("Average data availability = ", round(mean(result[,2]),1),"%"),size=4,
           x = as.Date(date[round(0.518*length(date))]), y= 80)
da <- da + annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 4,
           x = as.Date(date[round(0.518*length(date))]), y= 75)
da <- da + theme(plot.margin = unit(c(0.2,0.5,1,0.1),"cm"))#top,right,bottom,left

da
ggsave(da, filename = paste0(pathWrite,station,"_DA_LC.pdf"),width = 7.92, height = 5)

ug <- ggplot(result, aes(x=date)) + ylab('Uptime [%]') + theme_bw() + themesettings
ug <- ug + ggtitle(paste0("Uptime for ",station), subtitle= paste0("From ",date[1]," to ",date[length(date)]))
ug <- ug + theme(legend.position=c(.92,.15))
ug <- ug + geom_line(aes(y=uptime1, colour = "Uptime M1"))
ug <- ug + geom_line(aes(y=uptime2, colour = "Uptime M2"))
ug <- ug + geom_line(aes(y=uptime3, colour = "Uptime M3"), size = 0.4)
ug <- ug + scale_x_date(date_breaks = "month",date_labels = "%b")
ug <- ug + annotate('text', label = paste0('Average Uptime M1 = ', round(mean(result$uptime1, na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 71, colour = "red")
ug <- ug + annotate('text', label = paste0('Average Uptime M2 = ', round(mean(result$uptime2, na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 66, colour = "blue")
ug <- ug + annotate('text', label = paste0('Average Uptime M3 = ', round(mean(result$uptime3[57:length(result$uptime3)], na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 61, colour = "green")
ug <- ug + scale_colour_manual('Uptime', values = c("Uptime M1"="red", "Uptime M2"="blue", "Uptime M3" = "green"))   
ug

ggsave(ug, filename = paste0(pathWrite,station,"_Uptime.pdf"),width = 7.92, height = 5)

wup <- ggplot(result, aes(x=date)) + ylab("Weighted Uptime [%]") + theme_bw() + themesettings + theme(legend.position=c(.93,.15))
wup <- wup + ggtitle(paste0("Irradiance - weighted uptime for ", station), subtitle = paste0('From ', date[1], " to ", date[length(date)]))
wup <- wup + geom_line(aes(y=wuptime1, colour = "WUP M1"))
wup <- wup + geom_line(aes(y=wuptime2, colour = "WUP M2"))
wup <- wup + geom_line(aes(y=wuptime3, colour = "WUP M3"), size = 0.4)
wup <- wup + scale_x_date(date_breaks = "month",date_labels = "%b")
wup <- wup + annotate('text', label = paste0('Irradiance weighted uptime M1 = ', round(mean(result$wuptime1, na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 71, colour = "red")
wup <- wup + annotate('text', label = paste0('Irradiance weighted uptime  M2 = ', round(mean(result$wuptime2, na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 66, colour = "blue")
wup <- wup + annotate('text', label = paste0('Irradiance weighted uptime M3 = ', round(mean(result$wuptime3[57:length(result$wuptime3)], na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 61, colour = "green")
wup <- wup + scale_colour_manual('Uptime', values = c("WUP M1"="red", "WUP M2"="blue", "WUP M3" = "green"))   
wup

ggsave(wup, filename = paste0(pathWrite,station,"_weighted_Uptime.pdf"),width = 7.92, height = 5)

pr <- ggplot(result, aes(x=date)) + ylab("Performance Ratio [%]") + theme_bw() + themesettings + theme(legend.position=c(.93,.15))
pr <- pr + ggtitle(paste0(station, " Performance Ratio"), subtitle = paste0('From ', date[1], " to ", date[length(date)]))
pr <- pr + geom_line(aes(y=pr1, colour = "PR1"))
pr <- pr + geom_line(aes(y=pr2, colour = "PR2"))
pr <- pr + geom_line(aes(y=pr3, colour = "PR3"), size = 0.4)
pr <- pr + scale_x_date(date_breaks = "month",date_labels = "%b")
pr <- pr + annotate('text', label = paste0('Average PR M1 = ', round(mean(result$pr1, na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 61, colour = "red")
pr <- pr + annotate('text', label = paste0('Average PR M2 = ', round(mean(result$pr2[57:length(result$pr2)], na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 56, colour = "blue")
pr <- pr + annotate('text', label = paste0('Average PR M3 = ', round(mean(result$pr3[84:length(result$pr3)], na.rm =TRUE),1),' %'), size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y = 51, colour = "green")
pr <- pr + annotate("text",label = paste0("Lifetime = ", length(date)," days (",format(round(length(date)/365,1),nsmall = 1)," years)   "),size = 4,
                    x = as.Date(date[round(0.818*length(date))]), y= 46)
pr <- pr + scale_colour_manual('PR', values = c("PR1"="red", "PR2"="blue", "PR3" = "green"))   
pr


ggsave(pr, filename = paste0(pathWrite,station,"_PR v2.pdf"),width = 7.92, height = 5)

  



